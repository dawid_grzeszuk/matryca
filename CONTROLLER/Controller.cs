﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleCS.CONTROLLER
{
    public sealed class Controller
    {
        public void Start()
        {
            try
            {
                var csv = new CSV_READER.CsvReader();
                csv.ReadFile().Wait();
                new XMLmaker.XMLmaker().XmlMaker(csv.ListOfTransactions).Wait();
                new CsMaker().WriteFile(csv.ListOfTransactions).Wait();
                Console.WriteLine("wszystko ok");
            }
            catch { Console.WriteLine("error");}
            finally { Console.WriteLine("program wykonal swoj kod");}
        }
    }
}
