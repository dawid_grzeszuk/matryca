﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCS.CSV_READER
{
    public sealed class CsvReader
    {
        public List<string> ListOfTransactions { get; set; }

        public CsvReader()
        {
            ListOfTransactions = new List<string>();
        }

        public async Task ReadFile()
        {
            await Task.Run(() =>
            {
                using (var sr = new StreamReader(new FileStream("csv.csv", FileMode.Open, FileAccess.Read), Encoding.UTF8))
                {
                    while (sr.Peek()>-1)
                    {
                        var readLine = sr.ReadLine();
                        if (Equals(readLine,null)) continue;

                        foreach (var transaction in readLine.Split(',').Select((value, index) => new {index, value}))
                        {
                            if (transaction.index == 2) AddToList(transaction.value.Replace('"', ' ').TrimEnd().TrimStart());
                        }
                    }
                }
            });
        }
        private void AddToList(string arg)
        {
            if (!ListOfTransactions.Exists(x => x == arg)) ListOfTransactions.Add(arg);
        }

    }
}
