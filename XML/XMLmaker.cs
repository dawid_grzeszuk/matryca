﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLmaker
{
    public sealed class XMLmaker
    {
        public async Task XmlMaker(List<string> arg)
        {
            await Task.Run(()=>
            {
                lock (arg)
                {
                    using (var sw = new StreamWriter(new FileStream(@"output\matryce.xml", FileMode.Create, FileAccess.ReadWrite), Encoding.UTF8))
                    {
                        sw.WriteLine("<?xml version=\"1.0\" encoding=\"Unicode\"?>\r\n<session xmlns=\"http://www.soneta.pl/schema/business\">");
                        sw.WriteLine();

                        foreach (var template in arg.Select((value, index) => new { value, index }))
                        {
                            sw.WriteLine($"<MatrycaBase id=\"MatrycaBase_{template.index + 1}\" guid=\"{Guid.NewGuid()}\" class=\"Soneta.Ksiega.MatrycaWyplaty,Soneta.Ksiega\">");
                            sw.WriteLine(" <Typ>Wypłata</Typ>");
                            sw.WriteLine($" <Symbol>{template.value}</Symbol>");
                            sw.WriteLine(" <Definicja />");
                            sw.WriteLine(" <Opis />");
                            sw.WriteLine($" <Rozszerzenie>OpisZapłaty: PKO Inteligo {template.value}</Rozszerzenie>");
                            sw.WriteLine(" <Oddzial />");
                            sw.WriteLine(" <UserCodeEnabled>False</UserCodeEnabled>");
                            sw.WriteLine(" <UserCodeMethod />");
                            sw.WriteLine("</MatrycaBase>");
                            sw.WriteLine();
                        }
                    }
                }
            });
        }
    }
}
