﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCS
{
    public sealed class CsMaker
    {
        public async Task WriteFile(List<string> arg)
        {
            await Task.Run(() =>
            {
                lock (arg)
                {
                    using (var sw = new StreamWriter(new FileStream(@"output\code.cs", FileMode.Create, FileAccess.ReadWrite), Encoding.UTF8, 512))
                    {
                        sw.WriteLine("public override void RaportPrzedIdentyfikacja(RaportESP report)\r\n{");
                        sw.WriteLine("  foreach (var op in report.OperacjeBankowe)\r\n  {");
                        sw.WriteLine("   if (Equals(op.Tekst, null)) return;");
                        sw.WriteLine("   var matryca = Porownaj(op.Tekst.ToString().ToUpper());");
                        sw.WriteLine("   if (!Equals(matryca, null)) UruchomMatryce(op, matryca);\n  }\n}\n");

                        sw.WriteLine("private string Porownaj(string tekst)\n{");
                        foreach (var ifLine in arg)
                        {
                            sw.WriteLine($"   if (tekst.Contains(\"{ifLine}\")) return \"PKO Inteligo {ifLine}\";");
                        }
                        sw.WriteLine("   return null;");
                        sw.WriteLine("}");
                    }
                }
            });
        }

    }
}
